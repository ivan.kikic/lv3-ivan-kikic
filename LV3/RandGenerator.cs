﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3
{
    class RandGenerator
    {
        private static RandGenerator instance;
        private Random generator = new Random();
        private double[,] matrix;
        private int m, n;
        private RandGenerator()
        {
            matrix = new double[0,0];
        }
        public static RandGenerator GetInstance()
        {
            if (instance == null)
            {
                instance = new RandGenerator();
            }
            return instance;
        }
        public void fillMatrix()
        {
            int i, j;
            for (i = 0; i < m; i++)
            {
                for (j = 0; j < n; j++)
                {
                    matrix[i, j] = generator.NextDouble();
                }
            }
        }

        public void printMatrix()
        {
            int i, j;
            for (i = 0; i < m; i++)
            {
                for (j = 0; j < n; j++)
                {
                    Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
        }
        public void SetMatrix(int m, int n)
        {
            this.m = m;
            this.n = n;
            matrix = new double[m, n];
        }
    }
}
