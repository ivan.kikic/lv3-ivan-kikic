﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3
{
    class Logger
    {
        private String filePath;
        private static Logger instance;
        private Logger(string file)
        {
            this.filePath = file;
        }
        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger("ivan_kikic.txt");
            }
            return instance;
        }
        public String Filepath
        {
            get { 
                return filePath; 
            }
            set { 
                filePath = value; 
            }

        }
        public void Log(string message)
        {
            using (System.IO.StreamWriter fileWriter = new System.IO.StreamWriter(this.filePath, true))
            {
                fileWriter.WriteLine(message);
            }
        }
    }
}
