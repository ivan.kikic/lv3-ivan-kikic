﻿using System;

namespace LV3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Prvi zadatak");
            Dataset data1 = new Dataset("ivankikic.txt");
            Dataset data2 = (Dataset)data1.Clone();
            data1.Print();
            Console.WriteLine();
            data2.Print();
            Console.WriteLine("\nDrugi zadatak");
            RandGenerator matrix = RandGenerator.GetInstance();
            matrix.SetMatrix(3, 3);
            matrix.fillMatrix();
            matrix.printMatrix();

            ConsoleNotification Text1 = new ConsoleNotification("Ivan Kikic", "LV3", "Cetvrti zadatak", DateTime.Now, Category.INFO, ConsoleColor.Green);
            ConsoleNotification Text2 = new ConsoleNotification("Ivan Kikic", "Karantena", "Puno dana u karanteni :/", DateTime.Now, Category.ALERT, ConsoleColor.Red);
            NotificationManager Manager = new NotificationManager();

            Manager.Display(Text1);
            Console.WriteLine();
            Manager.Display(Text2);
        }
    }
}
